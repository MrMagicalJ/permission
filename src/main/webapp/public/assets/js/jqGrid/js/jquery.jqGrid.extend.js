/*********************************************************************************************************************
 *
 * 开发人员:	dd-Ping
 * 开发日期:	2017年10月27日
 * 
 * 修改人员:
 * 修改时间: 
 * 
 * 功能描述:	增加grid表格默认值
 * 			
 * 调用方式	
 *
*********************************************************************************************************************/
;
(function($) {
	$.jgrid = $.jgrid || {};
	$.extend($.jgrid, {
		defaults : {
		    recordtext : "显示 {0} - {1}，共 {2} 条", // 共字前是全角空格
		    emptyrecords : "暂无数据显示",
		    loadtext : "数据读取中...",
		    pgtext : " {0} 共 {1} 页",
		    rowNum : 20,
		    rowList: [20, 40, 60, 100],
		    altRows : true,
		    gridview : true,
		    page : 1,
		    datatype : "json",
		    mtype : "POST",
		    viewrecords : true,
		    shrinkToFit : true,
		    autowidth : true,
		    scroll : false,
		    pgbuttons : true,
		    multiselect : true,
		    rownumbers : true,
		    sortorder : "asc",
		    cmTemplate : {
			    sortable : false
		    },
		    jsonReader : {
		        root : "rows",
		        records : "records",
		        page : "page",
		        total : "total",
		        repeatitems : false
		    },
		    loadComplete : function() {
		    	var ts =this;
				onLoadComplete(ts.id);
			},
			onSelectRow : function(aRowids, status) {
				onSelectOneRow(aRowids, status);
			},
			onSelectAll : function(aRowids, status) {
				onSelectAllRow(aRowids, status);
			},
		    title : '数据表格',
		    onInitGrid : function() {
			   
		    }
		}
	});
})(jQuery);