package com.lwj.dto;

import com.google.common.collect.Lists;
import com.lwj.model.SysDept;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.BeanUtils;

import java.util.List;

/**
 * Created By lwj
 * 2018/3/1 0001 10:33
 */
@Getter
@Setter
@ToString
public class DeptLevelDto extends SysDept{

    private List<DeptLevelDto> deptList = Lists.newArrayList();

    /**
     * 对象copy的方法
     * @param sysDept
     * @return
     */
    public static DeptLevelDto adapt(SysDept sysDept) {
        DeptLevelDto dto = new DeptLevelDto();
        BeanUtils.copyProperties(sysDept, dto);
        return dto;
    }
}
