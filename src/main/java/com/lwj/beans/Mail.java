package com.lwj.beans;

import lombok.*;

import java.util.Set;

@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Mail {

    //  邮件主题
    private String subject;
    //  邮件内容
    private String message;
    //  收件人
    private Set<String> receivers;

}
