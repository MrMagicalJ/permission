package com.lwj.dto;


import com.google.common.collect.Lists;
import com.lwj.model.SysAclModule;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.BeanUtils;

import java.util.List;

@Getter
@Setter
@ToString
public class SysAclModuleDto extends SysAclModule {

    private List<SysAclModuleDto> aclModuleList = Lists.newArrayList();

    private List<AclDto> aclDtoList = Lists.newArrayList();

    /**
     * 对象copy的方法
     * @param sysAclModule
     * @return
     */
    public static SysAclModuleDto adapt(SysAclModule sysAclModule){
        SysAclModuleDto sysAclModuleDto = new SysAclModuleDto();
        BeanUtils.copyProperties(sysAclModule, sysAclModuleDto);
        return sysAclModuleDto;
    }
}
