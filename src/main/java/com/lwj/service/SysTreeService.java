package com.lwj.service;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.lwj.dao.SysAclMapper;
import com.lwj.dao.SysAclModuleMapper;
import com.lwj.dao.SysDeptMapper;
import com.lwj.dto.AclDto;
import com.lwj.dto.DeptLevelDto;
import com.lwj.dto.SysAclModuleDto;
import com.lwj.model.SysAcl;
import com.lwj.model.SysAclModule;
import com.lwj.model.SysDept;
import com.lwj.util.LevelUtil;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 计算树形结构
 * Created By lwj
 * 2018/3/1 0001 10:42
 */
@Service
public class SysTreeService {

    @Resource
    private SysDeptMapper sysDeptMapper;
    @Resource
    private SysAclModuleMapper sysAclModuleMapper;
    @Resource
    private SysCoreService sysCoreService;
    @Resource
    private SysAclMapper sysAclMapper;

    public List<SysAclModuleDto> roleTree(int roleId) {
        //1.当前用户分配过的权限点
        List<SysAcl> userAclList = sysCoreService.getCurrentUserAclList();
        //2.当前角色分配的权限点
        List<SysAcl> roleAclList = sysCoreService.getRoleAclList(roleId);
        //3.当前系统所有权限点
        List<AclDto> aclDtoList = Lists.newArrayList();

        Set<Integer> userAclIdSet = userAclList.stream().map(sysAcl -> sysAcl.getId()).collect(Collectors.toSet());
        Set<Integer> roleAclIdSet = roleAclList.stream().map(sysAcl -> sysAcl.getId()).collect(Collectors.toSet());

        //  获取所有的权限点
        List<SysAcl> allAclList = sysAclMapper.getAll();

        //  循环过滤设置，系统的所有权限点对当前用户来说，是否是有权限的，和是否是已勾选的
        for (SysAcl acl : allAclList) {
            AclDto dto = AclDto.adapt(acl);
            if (userAclIdSet.contains(acl.getId())) {
                dto.setHasAcl(true);
            }
            if (roleAclIdSet.contains(acl.getId())) {
                dto.setChecked(true);
            }
            aclDtoList.add(dto);
        }
        return aclListToTree(aclDtoList);
    }

    public List<SysAclModuleDto> aclListToTree(List<AclDto> aclDtoList) {
        if (CollectionUtils.isEmpty(aclDtoList)) {
            return Lists.newArrayList();
        }
        //  获取系统所有权限模块
        List<SysAclModuleDto> aclModuleDtoList = aclModuleTree();

        //  （权限模块id，权限点List）
        Multimap<Integer, AclDto> moduleIdAclMap = ArrayListMultimap.create();
        for (AclDto aclDto : aclDtoList) {
            if (aclDto.getStatus() == 1) {
                moduleIdAclMap.put(aclDto.getAclModuleId(), aclDto);
            }
        }

        bindAclsWithOrder(aclModuleDtoList, moduleIdAclMap);
        return aclModuleDtoList;
    }

    /**
     * 将权限点绑定到权限模块树上，并排序
     * @param aclModuleDtoList
     * @param moduleIdAclMap
     */
    public void bindAclsWithOrder(List<SysAclModuleDto> aclModuleDtoList, Multimap<Integer, AclDto> moduleIdAclMap) {
        if (CollectionUtils.isEmpty(aclModuleDtoList)) {
            return;
        }
        for (SysAclModuleDto dto : aclModuleDtoList) {
            List<AclDto> aclDtoList = (List<AclDto>) moduleIdAclMap.get(dto.getId());
            if (CollectionUtils.isNotEmpty(aclDtoList)) {
                Collections.sort(aclDtoList, aclSeqComparator);
                dto.setAclDtoList(aclDtoList);
            }
            bindAclsWithOrder(dto.getAclModuleList(), moduleIdAclMap);
        }
    }


    public List<SysAclModuleDto> aclModuleTree(){
        List<SysAclModule> sysAclModuleList = sysAclModuleMapper.getAllAclModule();
        List<SysAclModuleDto> dtoList = Lists.newArrayList();
        sysAclModuleList.forEach(sysAclModule -> {
            dtoList.add(SysAclModuleDto.adapt(sysAclModule));
        });
        return aclModuleListToTree(dtoList);
    }

    public List<SysAclModuleDto> aclModuleListToTree(List<SysAclModuleDto> dtoList){
        if (CollectionUtils.isEmpty(dtoList)) {
            return Lists.newArrayList();
        }

        Multimap<String, SysAclModuleDto> levelAclModuleMap = ArrayListMultimap.create();
        List<SysAclModuleDto> rootList = Lists.newArrayList();
        for (SysAclModuleDto sysAclModuleDto : dtoList) {
            levelAclModuleMap.put(sysAclModuleDto.getLevel(), sysAclModuleDto);
            if (sysAclModuleDto.getLevel().equals(LevelUtil.ROOT)) {
                rootList.add(sysAclModuleDto);
            }
        }
        //  按照seq从大小排序
        Collections.sort(rootList, aclModuleDtoComparator);
        //  递归生成树
        transformAclModuleTree(rootList, LevelUtil.ROOT, levelAclModuleMap);
        return rootList;
    }

    private void transformAclModuleTree(List<SysAclModuleDto> rootList, String level, Multimap<String,SysAclModuleDto> levelAclModuleMap) {
        for (SysAclModuleDto sysAclModuleDto : rootList) {
            //  计算当前跟节点的下一级节点的level值
            String nextLevel = LevelUtil.calculateLevel(level, sysAclModuleDto.getId());
            //  根据key值获取下一层级的节点
            List<SysAclModuleDto> tempList = (List<SysAclModuleDto>) levelAclModuleMap.get(nextLevel);
            if (CollectionUtils.isNotEmpty(tempList)) {
                Collections.sort(tempList, aclModuleDtoComparator);
                sysAclModuleDto.setAclModuleList(tempList);
                transformAclModuleTree(tempList, nextLevel, levelAclModuleMap);
            }
        }
    }


    /**
     * 获取部门树
     * @return
     */
    public List<DeptLevelDto> deptTree() {
        List<SysDept> deptList = sysDeptMapper.getAllDept();

        //将SysDept对象类型转换为DeptLevelDto对象类型
        List<DeptLevelDto> deptLevelDtoList = Lists.newArrayList();
        for (SysDept sysDept : deptList) {
            DeptLevelDto deptLevelDto = DeptLevelDto.adapt(sysDept);
            deptLevelDtoList.add(deptLevelDto);
        }
        return deptListToTree(deptLevelDtoList);
    }

    /**
     * 将部门List转换成部门树结构
     * @param deptLevelDtoList
     * @return
     */
    public List<DeptLevelDto> deptListToTree(List<DeptLevelDto> deptLevelDtoList) {
        if (CollectionUtils.isEmpty(deptLevelDtoList)) {
            return Lists.newArrayList();
        }
        // level -> [dept1, dept2, ...]
        Multimap<String, DeptLevelDto> levelDeptMap = ArrayListMultimap.create();
        List<DeptLevelDto> rootList = Lists.newArrayList();

        for (DeptLevelDto deptLevelDto : deptLevelDtoList) {
            levelDeptMap.put(deptLevelDto.getLevel(), deptLevelDto);
            if (LevelUtil.ROOT.equals(deptLevelDto.getLevel())) {
                rootList.add(deptLevelDto);
            }
        }
        //按照seq从小到大排序
        Collections.sort(rootList, deptSeqComparator);
        //递归生成树
        transformDeptTree(rootList, LevelUtil.ROOT, levelDeptMap);
        return rootList;
    }

    /**
     * 递归的方式将组装部门树结构类型
     * @param deptLevelDtoList
     * @param level
     * @param levelDeptMap
     */
    public void transformDeptTree(List<DeptLevelDto> deptLevelDtoList, String level, Multimap<String, DeptLevelDto> levelDeptMap) {
        for (int i = 0; i < deptLevelDtoList.size(); i++) {
            //遍历该层级的每个元素
            DeptLevelDto deptLevelDto = deptLevelDtoList.get(i);
            //处理当前层级的数据
            String nextLevel = LevelUtil.calculateLevel(level, deptLevelDto.getId());
            //处理下一层
            List<DeptLevelDto> tempDeptList = (List<DeptLevelDto>) levelDeptMap.get(nextLevel);
            if (CollectionUtils.isNotEmpty(tempDeptList)) {
                //排序
                Collections.sort(tempDeptList, deptSeqComparator);
                //设置下一层部门
                deptLevelDto.setDeptList(tempDeptList);
                //进入到下一层处理
                transformDeptTree(tempDeptList, nextLevel, levelDeptMap);
            }
        }
    }

    Comparator<DeptLevelDto> deptSeqComparator = (DeptLevelDto s1, DeptLevelDto s2) -> (s1.getSeq().compareTo(s2.getSeq()));

    Comparator<SysAclModuleDto> aclModuleDtoComparator = (SysAclModuleDto s1, SysAclModuleDto s2) -> (s1.getSeq().compareTo(s2.getSeq()));

    Comparator<AclDto> aclSeqComparator = (AclDto s1, AclDto s2) -> (s1.getSeq().compareTo(s2.getSeq()));

}
