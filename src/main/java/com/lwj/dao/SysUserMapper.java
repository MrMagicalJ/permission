package com.lwj.dao;

import com.lwj.beans.PageQuery;
import com.lwj.model.SysUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysUserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysUser record);

    int insertSelective(SysUser record);

    SysUser selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysUser record);

    int updateByPrimaryKey(SysUser record);

    SysUser findByKeyword(@Param("keyword") String username);

    int countByMail(@Param("mail") String mail,@Param("userId") Integer userId);

    int countByTelephone(@Param("telephone")String telephone,@Param("id") Integer id);

    int countByDeptId(int deptId);

    List<SysUser> getPageByDeptId(@Param("deptId")int deptId, @Param("pageQuery")PageQuery pageQuery);
}