package com.lwj.beans;

import lombok.Getter;

/**
 * Created By lwj
 * 2018/8/16 0016 13:53
 */
@Getter
public enum CacheKeyConstants {

    SYSTEM_ACLS,

    USER_ACLS,

}
