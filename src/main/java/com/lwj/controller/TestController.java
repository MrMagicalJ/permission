package com.lwj.controller;

import com.lwj.common.JsonData;
import com.lwj.exception.ParamException;
import com.lwj.exception.PermissionException;
import com.lwj.param.TestVo;
import com.lwj.util.BeanValidator;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

/**
 * Created By lwj
 * 2018/2/12 0012 11:13
 */
@Controller
@RequestMapping("/test")
@Slf4j
public class TestController {

    @RequestMapping("/test.page")
    public ModelAndView page() {
        return new ModelAndView("test");
    }

    @RequestMapping("/hello.json")
    @ResponseBody
    public JsonData hello(){
        log.info("hello");
        throw new PermissionException("test Exception");
//        return JsonData.success("hello, permission");
    }

    @RequestMapping("/validate.json")
    @ResponseBody
    public JsonData validate(TestVo vo) throws ParamException{
        log.info("validate");
        BeanValidator.check(vo);
//        try {
//            Map<String, String> map = BeanValidator.validateObject(vo);
//            if (MapUtils.isNotEmpty(map)) {
//                for (Map.Entry<String, String> entry : map.entrySet()) {
//                    log.info("{}->{}", entry.getKey(), entry.getValue());
//                }
//            }
//        } catch (Exception e) {
////            e.printStackTrace();
//        }
//        throw new PermissionException("test Exception");
        return JsonData.success("validate, permission");
    }
}
